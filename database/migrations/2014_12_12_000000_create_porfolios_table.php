<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('porfolios', function (Blueprint $table) {
            $table->id();
            $table->string('image1')->nullable();
            $table->string('titre1')->nullable();
            $table->string('description1')->nullable();
            $table->string('image2')->nullable();
            $table->string('titre2')->nullable();
            $table->string('description2')->nullable();
            $table->string('image3')->nullable();
            $table->string('titre3')->nullable();
            $table->string('description3')->nullable();
            $table->string('image4')->nullable();
            $table->string('titre4')->nullable();
            $table->string('description4')->nullable();
            $table->string('image5')->nullable();
            $table->string('titre5')->nullable();
            $table->string('description5')->nullable();
            $table->string('image6')->nullable();
            $table->string('titre6')->nullable();
            $table->string('description6')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('porfolios');
    }
};

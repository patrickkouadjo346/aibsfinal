<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        $user = User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
        ]);
        $user1 = User::factory()->create([
            'name' => 'User',
            'email' => 'user@user.com',
        ]);
        $role = Role::create(['name' => 'SuperAdmin']);
        $role1 = Role::create(['name' => 'Admin']);
        $user1->assignRole($role1);
        $user->assignRole($role);
    }
}

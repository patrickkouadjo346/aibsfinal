<?php

namespace App\Filament\Resources\OngletResource\Pages;

use App\Filament\Resources\OngletResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateOnglet extends CreateRecord
{
    protected static string $resource = OngletResource::class;
}

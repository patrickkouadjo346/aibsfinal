<?php

namespace App\Filament\Resources\OngletResource\Pages;

use App\Filament\Resources\OngletResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListOnglets extends ListRecords
{
    protected static string $resource = OngletResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}

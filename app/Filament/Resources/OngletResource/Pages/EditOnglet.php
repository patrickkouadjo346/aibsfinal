<?php

namespace App\Filament\Resources\OngletResource\Pages;

use App\Filament\Resources\OngletResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditOnglet extends EditRecord
{
    protected static string $resource = OngletResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}

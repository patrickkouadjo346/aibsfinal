<?php

namespace App\Filament\Resources\ChiffreResource\Pages;

use App\Filament\Resources\ChiffreResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateChiffre extends CreateRecord
{
    protected static string $resource = ChiffreResource::class;
}

<?php

namespace App\Filament\Resources\TemoinResource\Pages;

use App\Filament\Resources\TemoinResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTemoin extends CreateRecord
{
    protected static string $resource = TemoinResource::class;
}

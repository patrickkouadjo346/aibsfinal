<?php

namespace App\Filament\Resources\TemoinResource\Pages;

use App\Filament\Resources\TemoinResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListTemoins extends ListRecords
{
    protected static string $resource = TemoinResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}

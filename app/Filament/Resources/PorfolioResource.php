<?php

namespace App\Filament\Resources;

use App\Filament\Resources\PorfolioResource\Pages;
use App\Filament\Resources\PorfolioResource\RelationManagers;
use App\Models\Porfolio;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables;
use Filament\Forms\Components\FileUpload;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class PorfolioResource extends Resource
{
    protected static ?string $model = Porfolio::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('titre1')->label('Titre'),
                Forms\Components\TextInput::make('description1')->label('description'),
                FileUpload::make('image1')->label('Image'),
                Forms\Components\TextInput::make('titre2')->label('Titre1'),
                Forms\Components\TextInput::make('description2')->label('description1'),
                FileUpload::make('image2')->label('Image1'),
                Forms\Components\TextInput::make('titre3')->label('Titre2'),
                Forms\Components\TextInput::make('description3')->label('description2'),
                FileUpload::make('image2')->label('Image2'),
                Forms\Components\TextInput::make('titre4')->label('Titre3'),
                Forms\Components\TextInput::make('description4')->label('description3'),
                FileUpload::make('image4')->label('Image3'),
                Forms\Components\TextInput::make('titre5')->label('Titre4'),
                Forms\Components\TextInput::make('description5')->label('description4'),
                FileUpload::make('image5')->label('Image4'),
                Forms\Components\TextInput::make('titre6')->label('Titre5'),
                Forms\Components\TextInput::make('description6')->label('description5'),
                FileUpload::make('image6')->label('Image5'),

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('titre1')->label('Titre'),
                TextColumn::make('description1')->label('description'),
                ImageColumn::make('image1')->label('Image')->square(),
                TextColumn::make('titre2')->label('Titre1'),
                TextColumn::make('description2')->label('description1'),
                ImageColumn::make('image2')->label('Image1')->square(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPorfolios::route('/'),
            'create' => Pages\CreatePorfolio::route('/create'),
            'edit' => Pages\EditPorfolio::route('/{record}/edit'),
        ];
    }
}

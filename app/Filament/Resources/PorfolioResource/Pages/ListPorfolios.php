<?php

namespace App\Filament\Resources\PorfolioResource\Pages;

use App\Filament\Resources\PorfolioResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListPorfolios extends ListRecords
{
    protected static string $resource = PorfolioResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}

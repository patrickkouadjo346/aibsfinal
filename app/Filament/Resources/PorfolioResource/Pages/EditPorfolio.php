<?php

namespace App\Filament\Resources\PorfolioResource\Pages;

use App\Filament\Resources\PorfolioResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditPorfolio extends EditRecord
{
    protected static string $resource = PorfolioResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}

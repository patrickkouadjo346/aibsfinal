<?php

namespace App\Filament\Resources\PorfolioResource\Pages;

use App\Filament\Resources\PorfolioResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreatePorfolio extends CreateRecord
{
    protected static string $resource = PorfolioResource::class;
}

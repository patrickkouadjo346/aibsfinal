<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Porfolio extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'image1',
        'titre1',
        'description1',
        'image2',
        'titre2',
        'description2',
        'image3',
        'titre3',
        'description3',
        'image4',
        'titre4',
        'description4',
        'image5',
        'titre5',
        'description5',
        'image6',
        'titre6',
        'description6'

    ];
}
